package ua.kr.shokhirev.andrii.operations;

public class SubOperation implements Operation {

    @Override
    public double executeOperation(double numberOne, double numberTwo) {
        return numberOne - numberTwo;
    }
}
