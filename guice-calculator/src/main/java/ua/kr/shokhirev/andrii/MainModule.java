package ua.kr.shokhirev.andrii;

import com.google.inject.Binder;
import com.google.inject.Module;
import ua.kr.shokhirev.andrii.numbers.NumberOne;
import ua.kr.shokhirev.andrii.numbers.NumberTwo;
import ua.kr.shokhirev.andrii.numbers.OperationNumber;
import ua.kr.shokhirev.andrii.operations.AddOperation;
import ua.kr.shokhirev.andrii.operations.DivOperation;
import ua.kr.shokhirev.andrii.operations.MulOperation;
import ua.kr.shokhirev.andrii.operations.SubOperation;

public class MainModule implements Module {

    @Override
    public void configure(Binder binder) {
        binder.bind(NumberOne.class);
        binder.bind(NumberTwo.class);
        binder.bind(OperationNumber.class);
        binder.bind(AddOperation.class);
        binder.bind(SubOperation.class);
        binder.bind(MulOperation.class);
        binder.bind(DivOperation.class);
    }
}
