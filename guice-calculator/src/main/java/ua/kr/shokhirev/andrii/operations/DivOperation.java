package ua.kr.shokhirev.andrii.operations;

public class DivOperation implements Operation {

    @Override
    public double executeOperation(double numberOne, double numberTwo) {
        return numberOne / numberTwo;
    }
}
