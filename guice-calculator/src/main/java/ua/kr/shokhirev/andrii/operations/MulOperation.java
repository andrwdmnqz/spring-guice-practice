package ua.kr.shokhirev.andrii.operations;

public class MulOperation implements Operation {

    @Override
    public double executeOperation(double numberOne, double numberTwo) {
        return numberOne * numberTwo;
    }
}
