package ua.kr.shokhirev.andrii.operations;

import ua.kr.shokhirev.andrii.numbers.NumberOne;
import ua.kr.shokhirev.andrii.numbers.NumberTwo;
import ua.kr.shokhirev.andrii.numbers.OperationNumber;

import javax.inject.Inject;


public class ExecuteOperation {
    @Inject
    private NumberOne numberOne;
    @Inject
    private NumberTwo numberTwo;
    @Inject
    private OperationNumber operationNumber;
    @Inject
    private AddOperation addOperation;
    @Inject
    private SubOperation subOperation;
    @Inject
    private DivOperation divOperation;
    @Inject
    private MulOperation mulOperation;



    public double callNeededOperation() {
        if (operationNumber.getOperationNumber() == 0) {
            return addOperation.executeOperation(numberOne.getNumberOneValue(), numberTwo.getNumberTwoValue());
        }
        if (operationNumber.getOperationNumber() == 1) {
            return subOperation.executeOperation(numberOne.getNumberOneValue(), numberTwo.getNumberTwoValue());
        }
        if (operationNumber.getOperationNumber() == 2) {
            return mulOperation.executeOperation(numberOne.getNumberOneValue(), numberTwo.getNumberTwoValue());
        }
        if (operationNumber.getOperationNumber() == 3) {
            return divOperation.executeOperation(numberOne.getNumberOneValue(), numberTwo.getNumberTwoValue());
        }
        return 0;
    }
}
