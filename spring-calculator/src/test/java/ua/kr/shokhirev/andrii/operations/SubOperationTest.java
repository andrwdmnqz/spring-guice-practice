package ua.kr.shokhirev.andrii.operations;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class SubOperationTest {

    @Mock
    private SubOperation subOperation;

    @Test
    public void negativeArgumentInSubOperation() {
        double result = subOperation.executeOperation(1, -50);
        assertEquals(-49, result, 0.0);
    }
}
