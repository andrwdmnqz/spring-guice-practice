package ua.kr.shokhirev.andrii.numbers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class OperationNumberTest {

    @Mock
    private OperationNumber operationNumber;

    @Test(expected = IllegalArgumentException.class)
    public void negativeOperationNumberTest() {
        OperationNumber.setOperationNumber(-5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void greaterThanThreeOperationNumberTest() {
        OperationNumber.setOperationNumber(5);
    }
}
