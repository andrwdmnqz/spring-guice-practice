package ua.kr.shokhirev.andrii.operations;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DivOperationTest {

    @Mock
    private DivOperation divOperation;

    @Test(expected = IllegalArgumentException.class)
    public void secondOperandIsZero() {
        divOperation.executeOperation(5, 0);
    }
}
