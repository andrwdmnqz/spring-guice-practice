package ua.kr.shokhirev.andrii.numbers;

import org.springframework.stereotype.Component;

@Component
public class OperationNumber {
    private static int OperationNumber;

    public int getOperationNumber() {
        return OperationNumber;
    }

    public static void setOperationNumber(int operationNumber) {
        if (operationNumber < 0 || operationNumber > 3) {
            throw new IllegalArgumentException("Operation number cannot be less than zero or greater than three");
        }

        OperationNumber = operationNumber;
    }
}
