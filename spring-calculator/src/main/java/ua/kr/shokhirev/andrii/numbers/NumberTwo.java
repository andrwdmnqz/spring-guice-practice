package ua.kr.shokhirev.andrii.numbers;

import org.springframework.stereotype.Component;

@Component
public class NumberTwo {
    private static double NumberTwoValue;

    public double getNumberTwoValue() {
        return NumberTwoValue;
    }

    public static void setNumberTwoValue(double numberOneValue) {
        NumberTwoValue = numberOneValue;
    }


}