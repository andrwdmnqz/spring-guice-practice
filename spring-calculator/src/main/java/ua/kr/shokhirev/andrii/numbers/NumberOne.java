package ua.kr.shokhirev.andrii.numbers;

import org.springframework.stereotype.Component;

@Component
public class NumberOne {
    private static double NumberOneValue;

    public double getNumberOneValue() {
        return NumberOneValue;
    }

    public static void setNumberOneValue(double numberOneValue) {
        NumberOneValue = numberOneValue;
    }
}
