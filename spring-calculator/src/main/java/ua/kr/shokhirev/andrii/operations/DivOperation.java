package ua.kr.shokhirev.andrii.operations;

import org.springframework.stereotype.Component;

@Component
public class DivOperation implements Operation {

    @Override
    public double executeOperation(double numberOne, double numberTwo) {
        if (numberTwo == 0) {
            throw new IllegalArgumentException("Divisor cannot be zero");
        }
        return numberOne / numberTwo;
    }
}
